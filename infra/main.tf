terraform {
  backend "s3" {
    bucket  = "terraform-demo-franky"
    key     = "python-server.tfstate"
    region  = "eu-west-1"
    encrypt = true
  }
  required_version = ">= 0.12.0"
}

module "service" {
  source          = "git@gitlab.com:franky_armengol/terraform-aws-ecs-service-module.git"
  service_port    = var.service_port
  alb_port        = var.alb_port
  name            = var.name
  container_image = var.container_image
  environment = [
    {
      name  = "PORT"
      value = var.service_port
    }
  ]
}
