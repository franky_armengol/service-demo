variable "aws_region" {
  description = "Aws region to deploy all resources"
  type        = string
  default     = "eu-west-1"
}

variable "name" {
  description = "Name of the project"
  type        = string
}

variable "container_image" {
  description = "Name of the conainter image with his tag"
  type        = string
}

variable "service_port" {
  description = "Service internal port"
  default     = 3000
}

variable "alb_port" {
  description = "ALB external port"
  default     = 80
}
