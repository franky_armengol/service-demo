import os
from flask import Flask
app = Flask(__name__)

@app.route("/")
def home():
    return "Flask 1.1.1 Home page franky"

if __name__ == '__main__':
    app.run(threaded=True,host='0.0.0.0',port=os.environ['PORT'])
