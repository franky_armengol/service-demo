# Service Demo project

This project is used only for demo. Contains a python server using flask.

The objetive of this project is to automate the deployment of a microservice into aws ecs cluster using fargate.

## Test docker image in local

```
docker build -t service-demo .
docker run -d -p 80:3000 -e PORT=3000 service-demo
curl -L localhost
```

## Deployment

The deployment is executed by gitab-ci. You need to set the following CI/CD variables for the pipeline.

- AWS\_ACCESS\_KEY\_ID: Required for aws
- AWS\_SECRET\_ACCESS\_KEY: Required for aws
- AWS\_DEFAULT\_REGION: Required for aws
- AWS\_ACCOUNT\_ID: Required for push to AWS ECR

Every time you create a commit a new pipeline is executed. The process of the pipeline is the following:

- Build, tag, and push the docker image to AWS ECR. If the ECR not exist the pipeline creates it.
- Executes a terraform ecs service module for deploy the docker image on the ecs cluster.
- The tag of the container image is: ${CI\_COMMIT\_REF\_SLUG}-${CI\_COMMIT\_SHORT\_SHA}. Ex: master-6440c30a

## Dependencies

- [terraform-aws-ecs-service-module](https://gitlab.com/franky_armengol/aws-ecs-service-module/)(master)

## Terraform

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| name | Name of the project. | `string` | Filled by gitlab-ci | yes |
| container\_name | Name of the conainter image with his tag. | `string` | Filled by gitlab-ci | yes |
| aws\_region | Aws region to deploy all resources | `string` | `"eu-west-1"` | no |
| service\_port | Version of kubernetes cluster | `number` | `3000` | no |
| alb\_port | Instance size type of workers | `number` | `80` | no |


### Documentation

For generate inputs and outputs for documentation:

```bash
cd infra && terraform-docs markdown .
```

### Providers

| Name | Version |
|------|---------|
| aws | 2.70.0 |
| local | 1.4.0 |
| terraform | >= 0.12.0 |

## Authors

Module is maintained by [Francesc Armengol](https://gitlab.com/franky_armengol).
